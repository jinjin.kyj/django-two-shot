from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


@login_required
def list_receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    form = ReceiptForm(request.POST or None)
    if form.is_valid():
        receipt = form.save(commit=False)
        receipt.purchaser = request.user
        receipt.save()
        return redirect("home")

    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)


@login_required
def list_category(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/list_category.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    form = ExpenseCategoryForm(request.POST or None)
    if form.is_valid():
        category = form.save(commit=False)
        category.owner = request.user
        form.save()
        return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    form = AccountForm(request.POST or None)
    if form.is_valid():
        account = form.save(commit=False)
        account.owner = request.user
        form.save()
        return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
